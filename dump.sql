--
-- PostgreSQL database dump
--

-- Dumped from database version 12.1
-- Dumped by pg_dump version 12.1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: pgcrypto; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS pgcrypto WITH SCHEMA public;


--
-- Name: EXTENSION pgcrypto; Type: COMMENT; Schema: -; Owner:
--

COMMENT ON EXTENSION pgcrypto IS 'cryptographic functions';


--
-- Name: uuid-ossp; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS "uuid-ossp" WITH SCHEMA public;


--
-- Name: EXTENSION "uuid-ossp"; Type: COMMENT; Schema: -; Owner:
--

COMMENT ON EXTENSION "uuid-ossp" IS 'generate universally unique identifiers (UUIDs)';


SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: conversions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.conversions (
    id uuid DEFAULT public.uuid_generate_v1mc() NOT NULL,
    filename text NOT NULL,
    old_format text NOT NULL,
    new_format text NOT NULL,
    directory text NOT NULL
);


ALTER TABLE public.conversions OWNER TO postgres;

--
-- Name: formats; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.formats (
    id uuid DEFAULT public.uuid_generate_v1mc() NOT NULL,
    name text NOT NULL,
    type text NOT NULL
);


ALTER TABLE public.formats OWNER TO postgres;

--
-- Data for Name: conversions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.conversions (id, filename, old_format, new_format, directory) FROM stdin;
\.


--
-- Data for Name: formats; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.formats (id, name, type) FROM stdin;
6c7fe122-3e3b-11ea-8d2b-bb63141ca3f2	PNG	image/png
6c7ff9b4-3e3b-11ea-8d2b-937e81e123fb	BMP	image/bmp
6c7ffcf2-3e3b-11ea-8d2b-b36279186294	WEBM	video/webm
6c800300-3e3b-11ea-8d2b-8386d924ddf0	MP4	video/mp4
6c80060c-3e3b-11ea-8d2b-570173413d68	MOV	video/quicktime
6c80090e-3e3b-11ea-8d2b-cf6c5a1aa296	JPG	image/jpeg
\.


--
-- Name: conversions conversions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.conversions
    ADD CONSTRAINT conversions_pkey PRIMARY KEY (id);


--
-- Name: formats formats_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.formats
    ADD CONSTRAINT formats_pkey PRIMARY KEY (id);


--
-- PostgreSQL database dump complete
--
